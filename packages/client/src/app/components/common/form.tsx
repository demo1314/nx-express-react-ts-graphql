import React, { useEffect, useState } from 'react';
import { Input } from 'antd';

import { UserType } from '../../InterfacesAndTypes';



const inputStyles = {
    width: '100%',
    margin: '6px 0'
}

export const Form = ({ submit, sendUserData, user }: { sendUserData: Function, submit: boolean, user?: UserType }) => {

    const [newUser, setNewUser] = useState<UserType>({
        name: '',
        email: '',
        photo: ''
    });

    const clearForm = () => {
        setNewUser({ name: '', email: '', photo: '' });
    }

    const handleChage = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        setNewUser(
            {...newUser, [event.target.name]: value}
        )
    }

    useEffect(() => {if(submit) {handleAddUser(), clearForm()}}, [submit]);
    useEffect(() => {if(user) setNewUser(user)}, [user]);

    const handleAddUser = () => {
        sendUserData(newUser)
    }

  return (
    <>
        <div style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
            <Input style={inputStyles} type='text' name='name'  placeholder='name'  onChange={handleChage} value={newUser.name}  />
            <Input style={inputStyles} type='text' name='email' placeholder='email' onChange={handleChage} value={newUser.email} />
            <Input style={inputStyles} type='text' name='photo' placeholder='photo' onChange={handleChage} value={newUser.photo} />
        </div>
    </>
  );
}

