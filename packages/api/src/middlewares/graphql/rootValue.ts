import users from '../../fakeDB';
import { faker } from '@faker-js/faker';
import { UserType } from './types';

const createUser = (input) => {
    const id = faker.datatype.uuid();
    return { id, ...input }
}

type Limits = {
    limit: number | undefined
    offset: number | undefined
}

type UserList = {
  totalCount: number
  Users: UserType[]
}

export const rootValue = {

    getAllUsers: (limits?: Limits): UserList => {
        const {limit, offset} = limits;
        const totalCount = users.length;
        if (limit && offset != undefined) return { totalCount, Users: users.slice(offset, offset + limit)};
        else return { totalCount, Users: users };
    },

    getUser: ({id}): UserType => {
      return users.find(user => user.id == id);
    },

    addUser: ({input}): UserType => {
      const user = createUser(input);
      users.push(user);
      return user;
    },

    updateUser: ({ input }): UserType => {
      const idx =  users.findIndex(user => user.id == input.id);
      input.name ? users[idx].name = input.name : '';
      input.email ? users[idx].email = input?.email : '';
      input.photo ? users[idx].photo = input?.photo : '';
      return users[idx];
    },

    removeUser: ({input}): object => {
      const idx =  users.findIndex(user => user.id == input.id);
      if (idx >= 0) {
        users.splice(idx, 1);
        return { status: "success" };
      }
      else {
        throw new Error('User not found');
      }
    }

}