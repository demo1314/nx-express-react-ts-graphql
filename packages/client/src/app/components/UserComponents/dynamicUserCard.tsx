import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { Button } from 'antd';
import { DeleteFilled, EditFilled } from '@ant-design/icons';

import { UserType, SendUserDataType } from '../../InterfacesAndTypes';
import { REMOVE_USER } from '../../api';
import { UserCard } from './userCard';
import { UserSender } from './userSender';
import { Dialog } from '../common';



export const UserCardHoc = ({ user, doUpdate }: { user: UserType, doUpdate: Function }): JSX.Element => {

    const { id } = user;
    
    const [openAddUserDialog, setOpenAddUserDialog] = useState<boolean>(false);
    const [addUser, setAddUser] = useState<boolean>(false)
    const [removeUser] = useMutation(REMOVE_USER);

    const addUserConfirmHandle = () => {
        setAddUser(true);
    }
    
    const updateHandle = () => {
        setOpenAddUserDialog(false);
        setAddUser(false);
        doUpdate();
    }

    const removeUserDataHandle = async () => {
        removeUser({
            variables: {
                input: {
                    id: id
                }
            }
        })
        .then(({data}) =>{
             console.log('result: ', data);
        })
        .finally(() => {
            doUpdate();
        })
    }

    return (
        <>
            <UserCard user={user} />
            <div style={{display: 'flex', justifyContent: 'space-between', width: '80px'}}>
                <Button type='primary' icon={<EditFilled/>} onClick={() => setOpenAddUserDialog(true)} />
                <Button danger icon={<DeleteFilled/>} onClick={removeUserDataHandle} />
            </div>
            <Dialog title='udpdate user' visible={openAddUserDialog} onConfirm={addUserConfirmHandle} onCancel={() => {setOpenAddUserDialog(false), setAddUser(false)}}>
                <UserSender apiType={SendUserDataType.UPDATE} doUpdate={updateHandle} submit={addUser} user={user}/>
            </Dialog>
        </>
    );
}

