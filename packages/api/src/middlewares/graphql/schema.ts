const { buildSchema } = require('graphql');

export const schema = buildSchema(`
    type User {
        id: ID
        name: String
        photo: String
        email: String
    }

    type Status {
        status: String
    }

    type UserList {
        totalCount: Int
        Users: [User]
    }

    input UserInput {
        id: ID
        name: String
        photo: String
        email: String
    }

    type Query {
        getAllUsers(limit: Int, offset: Int): UserList
        getUser(id: ID): User
    }

    type Mutation {
        addUser(input: UserInput): User
        updateUser(input: UserInput): User
        removeUser(input: UserInput): Status
    }
`);
