import { Button } from 'antd';



export const Pagination = ({ page, setPage, total }: { page: number, total?: number, setPage: Function }) => {

    return (
        <>
            <div>
                <nav style={{ margin: '12px', width: '95%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Button disabled={!page} onClick={() => setPage((prev: number) => prev - 1)}>Prev</Button>
                    <span style={{ width: '50px', margin: '0 14px' }}>Page { (page + 1).toString() }</span>
                    <Button disabled={page + 1 == total} onClick={() => setPage((prev: number) => prev + 1)}>Next</Button>
                </nav>
            </div>
        </>
    );
}

