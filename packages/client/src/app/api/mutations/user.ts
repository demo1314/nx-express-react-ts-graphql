import { gql } from '@apollo/client';

export const ADD_USER = gql`
    mutation addUser($input: UserInput) {
        addUser(input: $input) {
            id,
            name, 
            email, 
            photo
        }
    }
`

export const UPDATE_USER = gql`
    mutation updateUser($input: UserInput) {
        updateUser(input: $input) {
            id,
            name,
            email,
            photo
        }
}
`

export const REMOVE_USER = gql`
    mutation removeUser($input: UserInput) {
        removeUser(input: $input) {
            status
        } 
    }
`