import { faker } from '@faker-js/faker';
import { UserType } from './middlewares/graphql/types';


const users = <UserType[]>[];

for(let i = 0; i < 100; i++) {
    users.push({
        id: faker.datatype.uuid(),
        name: faker.name.firstName(),
        email: faker.internet.email(),
        photo: faker.image.avatar()
    })
}

export default users;