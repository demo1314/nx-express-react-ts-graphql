import { ReactNode } from 'react';
import { Modal } from 'antd';



export const Dialog = ({    children, 
                            title,
                            visible,
                            onConfirm,
                            onCancel
                        }: { 
                            children: ReactNode, 
                            title: string, 
                            visible: boolean,
                            onConfirm: Function,
                            onCancel: Function
                        }) => {
                            
    return (
        <>
            <Modal  title={title.toUpperCase()}
                    style={{ top: 20 }}
                    visible={visible}
                    onOk={() => onConfirm()}
                    onCancel={() => onCancel()}
            >
                {children}
            </Modal>
        </>
    );
}

