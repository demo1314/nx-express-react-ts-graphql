import * as express from 'express';
import * as cors from 'cors';
import { graphqlHTTP } from 'express-graphql';
import  { schema, rootValue } from './middlewares/graphql';


const app = express();

app.use(cors());


app.use('/graphql', graphqlHTTP({
  graphiql: true,
  schema,
  rootValue
}))

app.get('/api', (req, res) => {
  res.send({ message: 'Welcome to api!' });
});

const port = process.env.NX_API_PORT || 8080;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/api`);
});
server.on('error', console.error);
