# Express react on ts graphql demo app

## To run apps need add .env file, For example  look env.example file

### Install
`yarn install`

### Run api-server
`npx nx serve api`

### Run react client server
`npx nx serve client`
