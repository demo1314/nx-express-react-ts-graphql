import { useMutation } from '@apollo/client';

import { ADD_USER, UPDATE_USER } from '../../api';
import { UserType, SendUserDataType } from '../../InterfacesAndTypes';
import { Form } from '../common'



export const UserSender = ({ apiType, doUpdate, submit, user }: { apiType: SendUserDataType, submit: boolean, doUpdate: Function, user?: UserType }) => {

    const [createUser] = useMutation(ADD_USER);
    const [udpateUser] = useMutation(UPDATE_USER);

    const checkAction = async (user: UserType) => {
        if(apiType == SendUserDataType.CREATE) {
            return createUser({
                variables: {
                    input: {
                        name: user.name,
                        email: user.email,
                        photo: user.photo
                    }
                }
            })
        }
        else return udpateUser({
            variables: {
                input: {
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    photo: user.photo
                }
            }
        })
    }

    const sendeUserData = async (newUserData: UserType) => {
        try{
            console.log('newUserData: ', newUserData);
            const result = await checkAction(newUserData);
            doUpdate()
        } catch(err) {
            console.error('sendeUserData error: ', err)
        }
    }


    return (
        <Form sendUserData={sendeUserData} submit={submit} user={user}/>
    )
}