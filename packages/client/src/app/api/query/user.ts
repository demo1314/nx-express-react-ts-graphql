import { gql } from '@apollo/client';

export const GET_ALL_USERS =  gql`    
    query {
        totalCount,
        Users {
            id,
            name,
            email,
            photo
        },
    }
`

export const GET_USERS_PAGE =  gql`    
    query getAllUsers ($limit: Int, $offset: Int) {
        getAllUsers(limit: $limit, offset: $offset) {
            totalCount,
            Users {
                id,
                name,
                email,
                photo
            }
        },
    }
`
