import { Avatar, List } from 'antd';

import { UserType } from '../../InterfacesAndTypes';



export const UserCard = ({ user }: { user: UserType }): JSX.Element => {

    return (
        <>
            <List.Item.Meta
                avatar={<Avatar src={user.photo} />}
                title={user.name}
                description={user.email}
            />
        </>
    );
}

