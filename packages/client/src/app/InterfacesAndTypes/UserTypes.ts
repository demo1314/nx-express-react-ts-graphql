export type UserType = {
    id?: string
    name: string
    email: string
    photo: string
}

export enum SendUserDataType {
    UPDATE,
    CREATE
}