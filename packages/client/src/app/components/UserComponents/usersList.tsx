import React, {  useEffect, useState } from 'react';
import { useQuery } from '@apollo/client';
import { List } from 'antd';

import { GET_USERS_PAGE } from '../../api';
import { UserType } from '../../InterfacesAndTypes';
import { Pagination } from '../common';
import { UserCardHoc } from './dynamicUserCard';



const PAGE_SIZE = 10

export const UserList = ({ doRefetch, onUpdate }: { doRefetch: boolean, onUpdate: Function }) => {

    const [users, setUsers] = useState<UserType[]>([]);
    const [page, setPage] = useState<number>(0);
    const [maxPage, setMaxPage] = useState<number>(0);
    const {data, loading, error, refetch} = useQuery(GET_USERS_PAGE, {
        variables: {
            limit: PAGE_SIZE,
            offset: page * PAGE_SIZE
        }
    });

    useEffect(() => {refetch()}, [doRefetch]);
    useEffect(() => {if(!loading) setUsers(data.getAllUsers.Users), colibrateMaxPage(data.getAllUsers.totalCount), onUpdate()}, [data]);

    const colibrateMaxPage = (total: number) => {
        setMaxPage(Math.ceil(total / PAGE_SIZE))
    }

    if (loading) return <h1> Loading ... </h1>
    if (error) return <h1>{error.message}</h1>
    else return (
        <>
            <div style={{ width: '500px' }}>
                <Pagination page={page} setPage={setPage} total={maxPage}/>
                <List
                    itemLayout='horizontal'
                    dataSource={users}
                    renderItem={(user: UserType, idx: number) => (
                        <List.Item>
                            <UserCardHoc key={idx} user={user} doUpdate={() => refetch()}/>
                        </List.Item>
                      )}
                >
                </List>
                <Pagination page={page} setPage={setPage} total={maxPage} />
            </div>
        </>
    );
}

