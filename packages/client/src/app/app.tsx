import { useState } from 'react';
import './App.css';
import { UserAddOutlined } from '@ant-design/icons';
import { Button } from 'antd';

import { UserList, Dialog, UserSender } from './components';
import { SendUserDataType } from './InterfacesAndTypes';



export function App() {

  const [refetchStatus, setRefetchStatus] = useState(false);
  const [openAddUserDialog, setOpenAddUserDialog] = useState<boolean>(false);
  const [addUser, setAddUser] = useState<boolean>(false)

  const addUserConfirmHandle = () => {
    setAddUser(true);
  }

  const updateHandle = () => {
    setOpenAddUserDialog(false);    
    setRefetchStatus(true);
    setAddUser(false);
  }

  return (
    <>
      <div style={{maxWidth: '1400px', width: '100%'}}>
        <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Button icon={<UserAddOutlined />} 
                    style={{ borderRadius: '50%', color: 'green', border: '1px solid green', margin: '30px' }}
                    onClick={() => setOpenAddUserDialog(true)}        
            ></Button>
        </div>
        <UserList doRefetch={refetchStatus} onUpdate={() => setRefetchStatus(false)} />
        <Dialog title='add user' visible={openAddUserDialog} onConfirm={addUserConfirmHandle} onCancel={() => {setOpenAddUserDialog(false), setAddUser(false)}}>
            <UserSender apiType={SendUserDataType.CREATE} doUpdate={updateHandle} submit={addUser}/>
        </Dialog>
      </div>
    </>
  );
}

export default App;
